#pragma once

#include <mutex>
#include <iostream>
#include <string>

template <typename T>
class WorkProgress
{
public:
	WorkProgress(const std::string& outputLabel, const T& totalWork, float outputPercentageInterval)
		: m_OutputLabel(outputLabel), m_TotalWork(totalWork), m_OutputInterval(static_cast<float>(m_TotalWork) * outputPercentageInterval)
	{}

	void submitWork(const T& work)
	{
		std::lock_guard<std::mutex> lock(m_Mutex);
		m_CurrentWork += work;
		outputChecker();
	}

private:
	void outputChecker()
	{
		if (m_CurrentWork - m_LastWorkOutput < m_OutputInterval) return;

		while(m_CurrentWork - m_LastWorkOutput >= m_OutputInterval)
			m_LastWorkOutput += m_OutputInterval;
		
		float percentage = static_cast<float>(m_CurrentWork) / static_cast<float>(m_TotalWork) * 100.f;
		std::cout << m_OutputLabel << ": " << std::fixed << std::setprecision(2) << percentage << "%.\n";
	}

private:
	std::string m_OutputLabel;
	T m_TotalWork;
	T m_CurrentWork = T(0);
	T m_LastWorkOutput = T(0);
	T m_OutputInterval;

	std::mutex m_Mutex;
};