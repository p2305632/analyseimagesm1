#pragma once

#include <opencv2/opencv.hpp>
#include "Algorithms/Utility.h"

#include "Data/Pixel.h"

class Image;

/**
 * @brief Static class for image transforming.
*/
class ImageProcessor
{
public:

	/**
	 * @brief Gets a stretched version of an image. Doesn't modify the image.
	 * @param a Lower pixel value.
	 * @param b Higher pixel value.
	 * @return A stretched version of the image.
	*/
	static Image Stretch(const Image& image, int a, int b);

	/**
	 * @brief Gets an equalized version of an image. Doesn't modify the image.
	 * @return An equalized version of the image.
	*/
	static Image Equalize(const Image& image);

	/**
	 * @brief Gets a filtered version of an image. Doesn't modify the image.
	 * @param f Convolution matrix to apply to the image. ONLY 3x3 with Floats depth!
	 * @return A filtered version of the image with grayscale depth.
	*/
	static Image Filter(const Image& image, const Image& f);

	/**
	 * @brief Gets a filtered version of an image. Doesn't modify the image.
	 * @param f Convolution matrix to apply to the image. ONLY 3x3 with Floats depth!
	 * @return A filtered version of the image with Floats depth.
	*/
	static Image FilterForValues(const Image& image, const Image& f);

	/**
	 * @brief Gets an image where each pixel at coordinates (r,c) is the euclidean norm of
	 * pixels at coordinates (r,c) from two images. Images are supposed to have the same
	 * dimensions and a grayscale/Floats color depth.
	 * @param image1 First image.
	 * @param image2 Second image.
	 * @return The computed image.
	*/
	static Image EuclideanDistance(const Image& image1, const Image& image2);

	/**
	 * @brief Gets an image where only pixels between lower and higher are kept. Other pixels are set
	 * to defaultPx. Only for grayscale depth.
	 * @param image Image to work on.
	 * @param lower Lower pixel color value.
	 * @param higher Higher pixel color value.
	 * @param defaultPx Pixel to set to pixels outside of defined bounds.
	 * @return 
	*/
	static Image KeepBetween(const Image& image, uchar lower, uchar higher, const Pixel& defaultPx = Pixel(uchar(0)));

	/**
	 * @brief Gets a thresholded image. Pixels of value lower than the threshold are set to the lower pixel.
	 * Pixels of value equal or greater than the threshold are set to the higher pixel.
	 * @param image The image to work on.
	 * @param threshold The threshold value.
	 * @param lowPx To assign when lower than the threshold.
	 * @param highPx To assign when higher than the threshold.
	 * @return 
	*/
	static Image Threshold(const Image& image, uchar threshold, const Pixel& lowPx, const Pixel& highPx);

	/**
	 * @brief Gets a region segmented image. Seeds are randomly planted on the image and then grow
	 * on neighbours. A seed will define a region, and then add neighbours to its region if neighbours
	 * pixel values are close enough to seed pixel value. /!\ Only for grayscale images.
	 * @param image The image to work on.
	 * @param seedsCount The number of seeds to plant on the image.
	 * @param neighbourRange How close a neighbour has to be from the seed to be added to its region.
	 * @return 
	*/
	static Image RegionGrowing(const Image& image, unsigned int seedsCount, unsigned int neighbourRange);

	static Image Dilatation(const Image& image);

};