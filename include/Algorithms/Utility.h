#pragma once

#include <unordered_set>

struct Coordinates
{
	int row = 0;
	int col = 0;

	bool operator==(const Coordinates& other) const
	{
		return row == other.row && col == other.col;
	}
};

template<>
struct std::hash<Coordinates>
{
	std::size_t operator()(const Coordinates& c) const noexcept
	{
		std::size_t h1 = std::hash<int>{}(c.row);
		std::size_t h2 = std::hash<int>{}(c.col);
		return h1 ^ (h2 << 1);
	}
};

class Utility
{
public:
	static std::unordered_set<Coordinates> GetNeighbours(const Coordinates& coords, int minRow, int minCol, int maxRow, int maxCol);

private:
	static bool AreCoordsInBounds(const Coordinates& coords, int minRow, int minCol, int maxRow, int maxCol);
};