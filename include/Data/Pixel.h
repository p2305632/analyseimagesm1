#pragma once

#include "Data/Depth.h"

class Pixel
{
public:
	/**
	 * @brief Constructs a pixel with default value (all channel at 0).
	 * @param depth Color depth.
	*/
	Pixel(const Depth& depth);

	/**
	 * @brief Constructs a single channel pixel with a specified color.
	 * @param col Color.
	*/
	Pixel(uchar col);

	/**
	 * @brief Constructs a 3 channels pixel with specified colors.
	 * @param r Red channel value.
	 * @param g Green channel value.
	 * @param b Blue channel value.
	*/
	Pixel(uchar r, uchar g, uchar b);

	/**
	 * @brief Constructs a single channel pixel with a specified value.
	 * @param value Value.
	*/
	Pixel(float value);

	/**
	 * @brief Gets pixel color. /!\ Only for grayscale color depth.
	 * @return The single channel color.
	*/
	uchar getColor() const;

	/**
	 * @brief Gets pixel value. /!\ Only for floats color depth.
	 * @return The single channel value.
	*/
	float getValue() const;

	/**
	 * @brief Gets the red channel value. /!\ Only for color color depth.
	 * @return The red channel value.
	*/
	uchar getR() const;

	/**
	 * @brief Gets the green channel value. /!\ Only for color color depth.
	 * @return The green channel value.
	*/
	uchar getG() const;

	/**
	 * @brief Gets the blue channel value. /!\ Only for color color depth.
	 * @return The blue channel value.
	*/
	uchar getB() const;

	/**
	 * @brief Sets the pixel color. /!\ Only for grayscale color depth.
	 * @param color New pixel color.
	*/
	void setColor(uchar color);

	/**
	 * @brief Sets the pixel value. /!\ Only for floats color depth.
	 * @param value New pixel value.
	*/
	void setValue(float value);

	/**
	 * @brief Sets the red channel color. /!\ Only for color color depth.
	 * @param color New red channel value.
	*/
	void setR(uchar color);

	/**
	 * @brief Sets the green channel color. /!\ Only for color color depth.
	 * @param color New green channel value.
	*/
	void setG(uchar color);

	/**
	 * @brief Sets the blue channel color. /!\ Only for color color depth.
	 * @param color New blue channel value.
	*/
	void setB(uchar color);

	/**
	 * @brief Gets the pixel color depth.
	 * @return The pixel color depth.
	*/
	const Depth& getDepth() const;

private:
	Depth m_Depth;
	uchar m_R, m_G, m_B;
	float m_Value;
};