#pragma once

#include <opencv2/opencv.hpp>

#include "Data/Pixel.h"
#include "Data/Depth.h"

#include "Data/Histogram.h"

class Histogram;

class Image
{
public:
	/**
	 * @brief Creates an empty image.
	 * @param width Image width.
	 * @param height Image height.
	 * @param depth Image depth.
	*/
	Image(int width, int height, const Depth& depth);

	/**
	 * @brief Creates an image from file.
	 * @param filePath Path of the image on disk.
	*/
	Image(const std::string& filePath, const Depth& depth);

	/**
	 * @brief Creates an image by copy.
	 * @param other The image to copy.
	*/
	Image(const Image& other);

	/**
	 * @brief Creates an Image from an OpenCV matrix.
	 * @param opencvMat OpenCV matrix const ref.
	*/
	Image(const cv::Mat& opencvMat);

	/**
	 * @brief Creates an Image from an OpenCV matrix.
	 * @param opencvMat OpenCV matrix rvalue.
	*/
	Image(cv::Mat&& opencvMat);

	/**
	 * @brief Checks if the image is empty (after a file loading for example).
	 * @return True if the image is empty ; false otherwise.
	*/
	bool isEmpty() const;

	/**
	 * @brief Gets the number of rows (lines).
	 * @return The number of rows.
	*/
	int getRowsCount() const;

	/**
	 * @brief Gets the number of columns.
	 * @return The number of columns.
	*/
	int getColsCount() const;

	/**
	 * @brief Gets a pixel from the Image.
	 * @param r Row index of pixel.
	 * @param c Column index of pixel.
	 * @return The desired pixel.
	*/
	Pixel getPixel(int r, int c) const;

	/**
	 * @brief Sets a pixel on the Image.
	 * @param r Rows index of pixel.
	 * @param c Column index of pixel.
	 * @param px The pixel to set.
	*/
	void setPixel(int r, int c, const Pixel& px);

	template <typename Container>
	void setPixels(const Container& container);

	/**
	 * @brief Returns Image depth.
	 * @return Image depth.
	*/
	const Depth& getDepth() const;

	/**
	 * @brief Gets the histogram of this image.
	 * @return The histogram.
	*/
	const Histogram& getHistogram() const;

	/**
	 * @brief Shows the image in a window.
	*/
	void show(const std::string& windowTitle) const;

private:
	void computeHistogram() const;
private:
	cv::Mat m_Mat;
	Depth m_Depth;

	mutable Histogram m_Hist;
	mutable bool m_HistComputed = false;
};

template<typename Container>
inline void Image::setPixels(const Container& container)
{
	int pxnb = getColsCount() * getRowsCount();
	for (int i = 0; i < container.size() && i < pxnb; i++)
	{
		int c = i % getRowsCount();
		int r = i / getColsCount();

		setPixel(r, c, container.at(i));
	}
}
