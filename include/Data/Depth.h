#pragma once

#include <opencv2/opencv.hpp>

enum class Depth
{
	GrayScale = 0,
	Color = 1,
	Floats = 2
};

int DepthToOpenCV(const Depth& depth);
Depth OpenCVToDepth(const int ocvDepth);

cv::Scalar DepthToDefaultScalar(const Depth& depth);