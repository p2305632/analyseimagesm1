#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <array>

class Image;

class Histogram
{
private:
    struct Value
    {
        int intensity = 0;
        float number = 0;
    };

    std::array<float, 256> values = { 0.f };
    Value max;
    Value left, right;

    int m_PxNumber = 0;

    bool m_Normalized = false;

public:

    Histogram() = default;
    Histogram(const Histogram& other) = default;

    void computeMax();
    void computeLeftAndRight();
    void compute(const Image& image);
    Histogram normalized() const;
    void show(const std::string& windowTitle, int height = 256) const;

    inline const std::array<float, 256>& getValues() const { return values; }
    inline const Value& getMaxValue() const { return max; }
    inline const Value& getLeftValue() const { return left; }
    inline const Value& getRightValue() const { return right; }
};

