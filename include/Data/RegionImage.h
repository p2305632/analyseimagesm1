#pragma once

#include <unordered_set>
#include <unordered_map>
#include "Algorithms/Utility.h"
#include <vector>
#include <mutex>

/**
 * @brief An image storing region indices for each pixel (for region growing).
*/
class RegionImage
{
public:
	/**
	 * @brief Creates a RegionImage with fixed sizes.
	 * @param width Number of columns.
	 * @param height Number of rows.
	*/
	RegionImage(unsigned int width, unsigned int height) noexcept;

	/**
	 * @brief Resets region indices for each pixel. Does not modify image sizes.
	*/
	void reset() noexcept;

	/**
	 * @brief Add a region index to a pixel. If the pixel already contains the region index,
	 * this function won't do anything. Thread safe function.
	 * @param row Pixel row index.
	 * @param col Pixel column index.
	 * @param regionIndex Region index to add to the pixel.
	*/
	void addRegionToPixel(unsigned int row, unsigned int col, int regionIndex);

	void setPixelRegion(unsigned int row, unsigned int col, int regionIndex);

	/**
	 * @brief Replaces a region index with another region index in each pixel.
	 * @param fromRegionIndex Region index to replace.
	 * @param toRegionIndex New region index to set.
	*/
	void replaceRegionBy(int fromRegionIndex, int toRegionIndex);

	/**
	 * @brief Get the regions indices associated with a certain pixel.
	 * @param row Pixel row index.
	 * @param col Pixel column index.
	 * @return A const reference to a set of regions indices.
	*/
	const std::unordered_set<int>& getPixelRegions(unsigned int row, unsigned int col) const;

	const std::unordered_set<Coordinates>& getRegionPixels(int region_Indx) const;

	std::unordered_set<int>& getPixelRegions(unsigned int row, unsigned int col);

private:
	unsigned int m_Width, m_Height;
	std::vector<std::unordered_set<int>> m_Pixels;
	std::unordered_map<int, std::unordered_set<Coordinates>>m_Regions;


	std::mutex m_Mutex;
};