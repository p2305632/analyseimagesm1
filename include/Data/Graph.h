#pragma once

#include <vector>
#include <unordered_set>

class Graph
{
public:
	struct Edge
	{
		Edge(bool connected, int weight)
			: connected(connected), weight(weight)
		{}

		Edge()
			: Edge(false, 0)
		{}

		bool connected = false;
		int weight = 0;
	};

public:
	Graph(size_t verticesCount);

	const Edge& getConnexion(size_t from, size_t to) const;
	bool isConnected(size_t from, size_t to) const;
	int getEdgeWeight(size_t from, size_t to) const;

	void setConnexion(size_t from, size_t to, bool connected, int weight, bool reversed = true);
	void connect(size_t from, size_t to, int weight, bool reversed = true);
	void disconnect(size_t from, size_t to, bool reversed = true);
	std::unordered_set<int> getNeighbours(size_t vertex) const;

	size_t getVerticesCount() const;

	std::vector<std::vector<int>> connectedSets() const;

private:
	size_t m_VerticesCount;

	std::vector<Edge> m_Adjacency;
};