﻿cmake_minimum_required (VERSION 3.8)
project ("AnalyseImage")

# Remplacer cet URL par l'URL de votre répertoire openCV (à la racine).
 #set(OPEN_CV_ROOT C:/Users/nassi/opencv)
set(OPEN_CV_ROOT C:/Users/beauj/Documents/Code/C++/libs/opencv)

include_directories(
	${OPEN_CV_ROOT}/build/include
	${PROJECT_SOURCE_DIR}/include
)

add_executable (AnalyseImage "src/AnalyseImage.cpp"  "src/Data/Image.cpp" "include/Data/Pixel.h" "src/Data/Pixel.cpp" "include/Data/Depth.h" "src/Data/Depth.cpp" "src/Data/Histogram.cpp"  "include/Data/Histogram.h" "include/Algorithms/ImageProcessor.h" "src/Algorithms/ImageProcessor.cpp" "include/Data/RegionImage.h" "src/Data/RegionImage.cpp" "include/Algorithms/Utility.h" "src/Algorithms/Utility.cpp" "include/Algorithms/WorkProgress.h" "include/Data/Graph.h" "src/Data/Graph.cpp")

if (CMAKE_VERSION VERSION_GREATER 3.12)
  set_property(TARGET AnalyseImage PROPERTY CXX_STANDARD 20)
endif()

target_link_libraries(AnalyseImage 
	optimized ${OPEN_CV_ROOT}/build/x64/vc16/lib/opencv_world481.lib
	debug ${OPEN_CV_ROOT}/build/x64/vc16/lib/opencv_world481d.lib
)
