# Projet d'analyse d'images

## Compilation

### Prérequis

- Système d'exploitation Windows 64 bits avec Visual Studio 17.
- Make, CMake.
- Binaries d'openCV pour Windows x64 (Visual Studio).

### Windows

- Cloner le dépôt en local :
`git clone git@forge.univ-lyon1.fr:p2305632/analyseimagesm1.git`
- Déplacez-vous à la racine du projet :
`cd analyseimagesm1`
- Dans le fichier CMakeLists.txt, modifiez le chemin de la librairie openCV pour qu'il pointe vers la racine de la librairie de votre installation.
- Créez un dossier build et déplacez-vous dedans :
```
mkdir build
cd build
```
- Génération de l'exécutable par cmake :
```
cmake -G "Visual Studio 17" -A x64 ..
cmake --build . --config Release
```
- L'exécutable a été placé à l'adresse analyseimagesm1/build/Release/AnalyseImage.exe. Déplacer les images .jpg depuis le dossier analyseimagesm1/ vers le dossier analyseimagesm1/build/Release/ .
- Il faudra également le fichier dll nécessaire à openCV. Dans le dossier {racine_openCV}/build/x64/vc16/bin, copiez le fichier opencv_world481.dll dans le même dossier que l'exécutable AnalyseImage.exe (attention à ne pas choisir le fichier se terminant par 'd').
- Lancez le programme :
```
cd Release
./AnalyseImage.exe
```
