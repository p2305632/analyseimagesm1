#include "Algorithms/Utility.h"

std::unordered_set<Coordinates> Utility::GetNeighbours(const Coordinates& coords, int minRow, int minCol, int maxRow, int maxCol)
{
	/*
		1 2 3
		4 5 6
		7 8 9
		5 = coords
		other indices = neighbours
	*/

	std::unordered_set<Coordinates> out;

	auto adder = [&](int row, int col)
	{
		Coordinates n = { row, col };
		if (AreCoordsInBounds(n, minRow, minCol, maxRow, maxCol))
			out.insert(n);
	};

	for (int r = -1; r <= 1; r++)
	{
		for (int c = -1; c <= 1; c++)
		{
			if (r == 0 && c == 0) continue;
			adder(coords.row + r, coords.col + c);
		}
	}

	return out;
}

bool Utility::AreCoordsInBounds(const Coordinates& coords, int minRow, int minCol, int maxRow, int maxCol)
{
	return coords.row >= minRow && coords.row <= maxRow
		&& coords.col >= minCol && coords.col <= maxCol;
}