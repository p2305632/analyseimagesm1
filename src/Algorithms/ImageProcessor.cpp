#include "Algorithms/ImageProcessor.h"

#include <thread>
#include <random>
#include <ctime>

#include "Data/Image.h"
#include "Data/RegionImage.h"
#include "Data/Graph.h"
#include "Algorithms/Utility.h"
#include "Algorithms/WorkProgress.h"

Image ImageProcessor::Stretch(const Image& image, int a, int b)
{
	Image out = { image };
	Histogram hist = image.getHistogram();
	const auto& left = hist.getLeftValue();
	const auto& right = hist.getRightValue();

	for (int i = 0; i < image.getRowsCount(); i++)
	{
		for (int j = 0; j < image.getColsCount(); j++)
		{
			int pxColor = static_cast<int>(image.getPixel(i, j).getColor());
			float numerator = static_cast<float>((b - a) * (pxColor - left.intensity));
			float denominator = static_cast<float>(right.intensity - left.intensity);
			uchar newColor = static_cast<uchar>(numerator / denominator + static_cast<float>(a));
			Pixel newPixel = { newColor };
			out.setPixel(i, j, newPixel);
		}
	}

	return out;
}

Image ImageProcessor::Equalize(const Image& image)
{
	Image out = { image };

	Histogram hist = image.getHistogram().normalized();
	const auto& values = hist.getValues();

	std::vector<float> cumulativeProbability(256, 0.0f);
	cumulativeProbability[0] = values[0];
	for (int i = 1; i < 256; i++)
	{
		cumulativeProbability[i] = cumulativeProbability[i - 1] + values[i];
	}

	float prefix = (float)(std::pow(2, 8) - 1);

	for (int i = 0; i < image.getRowsCount(); i++)
	{
		for (int j = 0; j < image.getColsCount(); j++)
		{
			const auto& intensity = image.getPixel(i, j).getColor();
			const auto& cumulativeProba = cumulativeProbability[intensity];
			uchar newIntensity = static_cast<uchar>(prefix * cumulativeProba);
			out.setPixel(i, j, newIntensity);
		}
	}

	return out;
}

Image ImageProcessor::Filter(const Image& image, const Image& f)
{
	Image out = { image.getColsCount(), image.getRowsCount(), image.getDepth() };

	for (int i = 1; i < image.getRowsCount() - 1; i++)
	{
		for (int j = 1; j < image.getColsCount() - 1; j++)
		{
			float newColor = 0.f;
			for (int ii = 0; ii < 3; ii++)
			{
				for (int jj = 0; jj < 3; jj++)
				{
					float imageColor = static_cast<float>(image.getPixel(i - 1 + ii, j - 1 + jj).getColor());
					float filterValue = f.getPixel(ii, jj).getValue();
					newColor += imageColor * filterValue;
				}
			}

			if (newColor < 0.f) newColor = 0.f;
			if (newColor > 255.f) newColor = 255.f;
			
			Pixel newPixel = { static_cast<uchar>(newColor) };
			out.setPixel(i, j, newPixel);
		}
	}

	return out;
}

Image ImageProcessor::FilterForValues(const Image& image, const Image& f)
{
	Image out = { image.getColsCount(), image.getRowsCount(), Depth::Floats };

	for (int i = 1; i < image.getRowsCount() - 1; i++)
	{
		for (int j = 1; j < image.getColsCount() - 1; j++)
		{
			float newColor = 0.f;
			for (int ii = 0; ii < 3; ii++)
			{
				for (int jj = 0; jj < 3; jj++)
				{
					float imageColor = static_cast<float>(image.getPixel(i - 1 + ii, j - 1 + jj).getColor());
					float filterValue = f.getPixel(ii, jj).getValue();
					newColor += imageColor * filterValue;
				}
			}

			Pixel newPixel = { newColor };
			out.setPixel(i, j, newPixel);
		}
	}

	return out;
}

Image ImageProcessor::EuclideanDistance(const Image& image1, const Image& image2)
{
	if (image1.getColsCount() != image2.getColsCount()
		|| image1.getRowsCount() != image2.getRowsCount())
	{
		throw std::exception("Images must have same dimensions.");
	}

	if ((image1.getDepth() != Depth::GrayScale && image1.getDepth() != Depth::Floats)
		|| (image2.getDepth() != Depth::GrayScale && image2.getDepth() != Depth::Floats))
	{
		throw std::exception("Images must have a grayscale or Floats color depth.");
	}

	const int width = image1.getColsCount();
	const int height = image1.getRowsCount();

	Image out(width, height, Depth::GrayScale);

	for (int r = 0; r < height; r++)
	{
		for (int c = 0; c < width; c++)
		{
			float value1 = 0.f;
			float value2 = 0.f;

			if (image1.getDepth() == Depth::GrayScale)
				value1 = static_cast<float>(image1.getPixel(r, c).getColor());
			else if(image1.getDepth() == Depth::Floats)
				value1 = image1.getPixel(r, c).getValue();

			if (image2.getDepth() == Depth::GrayScale)
				value2 = static_cast<float>(image2.getPixel(r, c).getColor());
			else if (image2.getDepth() == Depth::Floats)
				value2 = image2.getPixel(r, c).getValue();

			float newValue = std::sqrt(value1 * value1 + value2 * value2);
			if (newValue < 0.f) newValue = 0.f;
			if (newValue > 255.f) newValue = 255.f;

			out.setPixel(r, c, Pixel(static_cast<uchar>(newValue)));
		}
	}

	return out;
}

Image ImageProcessor::KeepBetween(const Image& image, uchar lower, uchar higher, const Pixel& defaultPx)
{
	if (image.getDepth() != Depth::GrayScale)
		throw std::exception("Image must have a grayscale color depth.");

	Image out = { image };

	for (int r = 0; r < image.getRowsCount(); r++)
	{
		for (int c = 0; c < image.getColsCount(); c++)
		{
			uchar color = image.getPixel(r, c).getColor();
			if (color < lower || color > higher)
				out.setPixel(r, c, defaultPx);
			else
				out.setPixel(r, c, image.getPixel(r, c));
		}
	}

	return out;
}

Image ImageProcessor::Threshold(const Image& image, uchar threshold, const Pixel& lowPx, const Pixel& highPx)
{
	if (image.getDepth() != Depth::GrayScale)
		throw std::exception("Image must have a grayscale color depth.");

	Image out = { image };

	for (int r = 0; r < image.getRowsCount(); r++)
	{
		for (int c = 0; c < image.getColsCount(); c++)
		{
			if (image.getPixel(r, c).getColor() < threshold)
				out.setPixel(r, c, lowPx);
			else
				out.setPixel(r, c, highPx);
		}
	}

	return out;
}

Image ImageProcessor::RegionGrowing(const Image& image, unsigned int seedsCount, unsigned int neighbourRange)
{
	// Useful data.
	const int cols = image.getColsCount();
	const int rows = image.getRowsCount();
	const int width = cols;
	const int height = rows;

	// Defining seeds (row, col).
	std::vector<Coordinates> seeds;
	seeds.reserve(seedsCount);

	std::srand(std::time(nullptr));

	for (int i = 0; i < seedsCount; i++)
	{
		bool ok = false;
		Coordinates seed;

		while (!ok)
		{
			seed = { std::rand() % rows, std::rand() % cols };

			if (std::find(seeds.begin(), seeds.end(), seed) == seeds.end())
				ok = true;
		}

		seeds.push_back(seed);
	}

	RegionImage regImg(width, height);
	WorkProgress<int> progress("Regions segmentation progression", seedsCount, 0.05f);

	auto work = [&](int startSeed, int endSeed)
	{
		for (int regionIndex = startSeed; regionIndex <= endSeed; regionIndex++)
		{
			Coordinates seed = { seeds[regionIndex].row, seeds[regionIndex].col };
			// First pixel to add to the region is the seed.
			regImg.addRegionToPixel(seed.row, seed.col, regionIndex);
			std::unordered_set<Coordinates> region = { seed };
			std::unordered_set<Coordinates> neighbours = Utility::GetNeighbours(seed, 0, 0, rows - 1, cols - 1);

			// Pixel colors of neighbours
			uchar seedColor = image.getPixel(seed.row, seed.col).getColor();
			uchar minColor = neighbourRange > seedColor ? 0 : seedColor - neighbourRange;
			uchar maxColor = neighbourRange > (255 - seedColor) ? 255 : seedColor + neighbourRange;

			bool oneAdded = true;

			// Adding neighbours sequentially
			// When we can't add a single neighbour to the region, the loop ends.
			while (oneAdded)
			{
				oneAdded = false;

				std::unordered_set<Coordinates> justAdded;

				// For each neighbour, we check if we can add it to the region.
				// If we can, we add it then we add its neighbours to the neighbours.
				for (const auto& neighbour : neighbours)
				{
					uchar neighbourColor = image.getPixel(neighbour.row, neighbour.col).getColor();

					// If neighbour color doesn't fit the region requirements, we don't add it.
					if (neighbourColor < minColor || neighbourColor > maxColor)
						continue;

					oneAdded = true;
					region.insert(neighbour);
					regImg.addRegionToPixel(neighbour.row, neighbour.col, regionIndex);
					justAdded.insert(neighbour);
				}

				// Filling the neighbours list.
				neighbours.clear();
				for (const auto& px : justAdded)
				{
					auto pxNeighbours = Utility::GetNeighbours(px, 0, 0, rows - 1, cols - 1);
					for (const auto& n : pxNeighbours)
						if (!region.contains(n))
							neighbours.insert(n);
				}
			}

			//std::cout << "Region " + std::to_string(regionIndex) + " done.\n";
			progress.submitWork(1);
		}

		//std::cout << "Worker done.\n";
	};

	// Getting number of physical cores on machine.
	unsigned int CPUCores = std::thread::hardware_concurrency();
	if (CPUCores == 0) CPUCores = 1; // Because it may return 0 when unable to detect.
	//CPUCores = 1;

	std::cout << "Using " << CPUCores << " CPU cores." << std::endl;
	std::cout << std::endl;

	std::vector<std::thread> workers;

	for (int i = 0; i < CPUCores; i++)
	{
		int startSeed = i * seedsCount / CPUCores;
		int endSeed = (i + 1) * seedsCount / CPUCores - 1;
		if (endSeed < 0 || endSeed > seedsCount - 1) endSeed = seedsCount - 1;

		std::cout << "Start of thread " + std::to_string(i) + ": " + std::to_string(startSeed) + ", end : " + std::to_string(endSeed) + "\n";

		workers.push_back(std::thread(work, startSeed, endSeed));

		if (endSeed == seedsCount - 1) break;
	}

	for (auto& worker : workers)
		worker.join();

	std::cout << std::endl;
	std::cout << "Region growing done. Start merging regions.\n";

	// Now we have to join regions

	// Constructing regions graph
	std::cout << "Constructing regions graph..." << std::endl;
	WorkProgress<int> regionsGraphProgress("Graph construction progression", seedsCount, 0.05f);
	Graph regionsGraph(seedsCount);
	
	for (int region = 0; region < seedsCount; region++)
	{
		const std::unordered_set<Coordinates>& pixels = regImg.getRegionPixels(region);

		for (const auto& px : pixels)
		{
			const std::unordered_set<int>& regions = regImg.getPixelRegions(px.row, px.col);

			for (const int& pxRegion : regions)
			{
				regionsGraph.connect(region, pxRegion, 0);
			}
		}

		regionsGraphProgress.submitWork(1);
	}

	// Merging

	auto connectedSets = regionsGraph.connectedSets();

	/* Printing connected sets on std out.
	std::cout << "Connected sets :" << std::endl;
	for (int i = 0; i < connectedSets.size(); i++)
	{
		std::cout << "Set " << i << ": ";
		for (int v = 0; v < connectedSets[i].size(); v++)
		{
			std::cout << connectedSets[i][v] << " ";
		}
		std::cout << std::endl;
	}
	*/


	// Coloring output image

	std::cout << "Coloring output image.";
	std::cout << std::endl;

	Image out(width, height, Depth::Color);

	for (int setIndex = 0; setIndex < connectedSets.size(); setIndex++)
	{
		const auto& set = connectedSets[setIndex];
		std::array<uchar, 3> color = { static_cast<uchar>(std::rand() % 256), static_cast<uchar>(std::rand() % 256), static_cast<uchar>(std::rand() % 256) };
		Pixel px = { color[0], color[1], color[2] };

		for (int regionIndex = 0; regionIndex < set.size(); regionIndex++)
		{
			int region = set[regionIndex];

			for (const Coordinates& coords : regImg.getRegionPixels(region))
			{
				out.setPixel(coords.row, coords.col, px);
			}
		}
	}

	std::cout << "Regions number: " << connectedSets.size() << std::endl;

	return out;
}


Image ImageProcessor::Dilatation(const Image& image)
{
	int kernel[3][3] = { {1,1,1},
						{1,1,1},
						{1,1,1} };
	Image out = { image.getColsCount(), image.getRowsCount(), image.getDepth() };
	
	for (int i = 0; i < image.getRowsCount(); i++)
	{
		for (int j = 0; j < image.getColsCount(); j++)
		{
			//Pixel pix(uchar (0))  = image.getPixel(i, j);
			if (image.getPixel(i, j).getColor() > 0)
			{
					// Appliquer le noyau de dilatation
					for (int ii = -1; ii <= 1; ii++)
					{
						for (int jj = -1; jj <= 1; jj++)
						{
							if (kernel[ii + 1][jj + 1] == 1)
							{
								
								out.setPixel(i + ii, j + jj, Pixel(uchar (255)));
							}
						}
					}
			}
		}
	}
	return out;

}
