#include "Data/Depth.h"

#include <opencv2/core/core.hpp>

int DepthToOpenCV(const Depth& depth)
{
	switch (depth)
	{
	case Depth::GrayScale:
		return CV_8UC1;
		break;
	case Depth::Color:
		return CV_8UC3;
		break;
	case Depth::Floats:
		return CV_32FC1;
		break;
	default:
		throw "Unsupported depth.";
		break;
	}
}

Depth OpenCVToDepth(int ocvDepth)
{
	switch (ocvDepth)
	{
	case CV_8UC1:
		return Depth::GrayScale;
		break;
	case CV_8UC3:
		return Depth::Color;
		break;
	case CV_32FC1:
		return Depth::Floats;
		break;
	default:
		throw "Unsupported depth.";
		break;
	}
}

cv::Scalar DepthToDefaultScalar(const Depth& depth)
{
	switch (depth)
	{
	case Depth::GrayScale:
		return cv::Scalar(0);
		break;
	case Depth::Color:
		return cv::Scalar(0, 0, 0);
		break;
	case Depth::Floats:
		return cv::Scalar(0.f);
		break;
	default:
		throw "Unsupported depth.";
		break;
	}
}
