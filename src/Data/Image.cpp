#include "Data/Image.h"

#include <stdexcept>

Image::Image(int width, int height, const Depth& depth)
	: m_Depth(depth)
{
	m_Mat = cv::Mat(height, width, DepthToOpenCV(depth), DepthToDefaultScalar(depth));
}

Image::Image(const std::string& filePath, const Depth& depth)
	: m_Mat(cv::imread(filePath, DepthToOpenCV(depth))), m_Depth(depth)
{

}

Image::Image(const Image& other)
	: m_Mat(other.m_Mat), m_Depth(other.m_Depth)
{
}

Image::Image(const cv::Mat& opencvMat)
	: m_Mat(opencvMat), m_Depth(OpenCVToDepth(opencvMat.depth()))
{
}

Image::Image(cv::Mat&& opencvMat)
	: m_Mat(std::move(opencvMat)), m_Depth(OpenCVToDepth(m_Mat.depth()))
{
}

bool Image::isEmpty() const
{
	return m_Mat.empty();
}

int Image::getRowsCount() const
{
	return m_Mat.rows;
}

int Image::getColsCount() const
{
	return m_Mat.cols;
}

Pixel Image::getPixel(int r, int c) const
{
	switch (m_Depth)
	{
	case Depth::GrayScale:
		return Pixel(m_Mat.at<uchar>(r, c));
		break;
	case Depth::Floats:
		return Pixel(m_Mat.at<float>(r, c));
		break;
	case Depth::Color:
	{
		const cv::Vec3b& opencvCol = m_Mat.at<cv::Vec3b>(r, c);
		return Pixel(opencvCol[2], opencvCol[1], opencvCol[0]);
	}
		break;
	default:
		throw std::exception("Unsupported depth.");
	}
}

void Image::setPixel(int r, int c, const Pixel& px)
{
	if (m_Depth != px.getDepth())
		throw std::exception("Pixel color depth has to match Image color depth.");


	switch (m_Depth)
	{
	case Depth::GrayScale:
		m_Mat.at<uchar>(r, c) = px.getColor();
		break;
	case Depth::Color:
		m_Mat.at<cv::Vec3b>(r, c) = cv::Vec3b(px.getB(), px.getG(), px.getR());
		break;
	case Depth::Floats:
		m_Mat.at<float>(r, c) = px.getValue();
		break;
	default:
		throw std::exception("Unsupported depth.");
		break;
	}

	m_HistComputed = false;
}

const Depth& Image::getDepth() const
{
	return m_Depth;
}

const Histogram& Image::getHistogram() const
{
	computeHistogram();
	return m_Hist;
}

void Image::computeHistogram() const
{
	if (m_HistComputed) return;

	m_Hist.compute(*this);
	m_HistComputed = true;
}

void Image::show(const std::string& windowTitle) const
{
	cv::imshow(windowTitle, m_Mat);
}