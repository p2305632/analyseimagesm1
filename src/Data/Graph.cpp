#include "Data/Graph.h"

#include <unordered_set>
#include <stack>

Graph::Graph(size_t verticesCount)
	: m_VerticesCount(verticesCount)
{
	m_Adjacency.assign(m_VerticesCount * m_VerticesCount, Edge());
}

size_t Graph::getVerticesCount() const
{
	return m_VerticesCount;
}

void Graph::setConnexion(size_t from, size_t to, bool connected, int weight, bool reversed)
{
	const size_t& row = from;
	const size_t& col = to;

	m_Adjacency.at(row * m_VerticesCount + col) = Edge(connected, weight);
	if (reversed)
		setConnexion(to, from, connected, weight, false);
}

void Graph::connect(size_t from, size_t to, int weight, bool reversed)
{
	setConnexion(from, to, true, weight, reversed);
}

void Graph::disconnect(size_t from, size_t to, bool reversed)
{
	setConnexion(from, to, false, 0, reversed);
}

const Graph::Edge& Graph::getConnexion(size_t from, size_t to) const
{
	const size_t& row = from;
	const size_t& col = to;

	return m_Adjacency.at(row * m_VerticesCount + col);
}

bool Graph::isConnected(size_t from, size_t to) const
{
	return getConnexion(from, to).connected;
}

int Graph::getEdgeWeight(size_t from, size_t to) const
{
	return getConnexion(from, to).weight;
}

std::unordered_set<int> Graph::getNeighbours(size_t vertex) const
{
	std::unordered_set<int> out;

	for (size_t i = 0; i < m_VerticesCount; i++)
	{
		if (isConnected(vertex, i))
			out.insert(i);
	}

	return out;
}

std::vector<std::vector<int>> Graph::connectedSets() const
{
	std::vector<std::vector<int>> connectedSets;
	std::unordered_set<int> verticesList;
	for (int i = 0; i < m_VerticesCount; i++)
		verticesList.insert(i);

	while (verticesList.size() > 0)
	{
		connectedSets.push_back(std::vector<int>());

		int vertex = *verticesList.begin();

		std::unordered_set<int> marked;
		std::stack<int> stack;
		stack.push(vertex);
		
		while (stack.size() > 0)
		{
			vertex = stack.top();
			stack.pop();

			if (!marked.contains(vertex))
			{
				marked.insert(vertex);
				connectedSets.back().push_back(vertex);
				verticesList.erase(vertex);

				for (const auto& neighbour : getNeighbours(vertex))
				{
					if (!marked.contains(neighbour))
						stack.push(neighbour);
				}
			}
		}
	}

	return connectedSets;
}