#pragma once

#include "Data/Histogram.h"

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <array>

#include "Data/Image.h"

void Histogram::computeMax()
{
	max.intensity = 0;
	max.number = 0;
	for (int i = 0; i < 256; i++)
	{
		if (values[i] > max.number)
		{
			max.intensity = i;
			max.number = values[i];
		}
	}
}

void Histogram::computeLeftAndRight()
{
	bool hasFoundNotNull = false;

	for (int i = 0; i < 256; i++)
	{
		if (values[i] > 0.f)
			hasFoundNotNull = true;

		if (!hasFoundNotNull)
		{
			left.intensity = i;
			left.number = 0.f;
		}
	}

	hasFoundNotNull = false;

	for (int i = 255; i >= 0; i--)
	{
		if (values[i] > 0.f)
			hasFoundNotNull = true;

		if (!hasFoundNotNull)
		{
			right.intensity = i;
			right.number = 0.f;
		}
	}
}

void Histogram::compute(const Image& image)
{
	m_Normalized = false;
	m_PxNumber = image.getRowsCount() * image.getColsCount();
	for (int i = 0; i < 256; i++)
		values[i] = 0.f;

	for (int i = 0; i < image.getRowsCount(); i++)
	{
		for (int j = 0; j < image.getColsCount(); j++)
		{
			auto px = image.getPixel(i, j);
			values[px.getColor()] += 1.f;
		}
	}

	computeMax();
	computeLeftAndRight();
}

Histogram Histogram::normalized() const
{
	if (m_Normalized) return *this;

	Histogram out;
	out.m_PxNumber = m_PxNumber;

	float pxNb = (float)m_PxNumber;

	for (int i = 0; i < 256; i++)
	{
		out.values[i] = this->values[i] / pxNb;
	}

	out.m_Normalized = true;
	out.computeMax();
	out.computeLeftAndRight();

	return out;
}

void Histogram::show(const std::string& windowTitle, int height) const
{
	cv::Mat img(height, 256, CV_8UC3, cv::Scalar(0, 0, 0));

	for (int i = 0; i < 256; i++)
	{
		cv::line(
			img, cv::Point(i, height), 
			cv::Point(i, (int)((1.f - (float)values[i] / (m_Normalized ? 1.f : (float)max.number)) * (float)height)), 
			cv::Scalar(255, 255, 255), 1, 8, 0);
	}

	cv::imshow(windowTitle, img);
}


