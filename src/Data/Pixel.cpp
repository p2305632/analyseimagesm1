#include "Data/Pixel.h"

Pixel::Pixel(const Depth& depth)
	: m_Depth(depth), m_R(0), m_G(0), m_B(0), m_Value(0.f)
{
}

Pixel::Pixel(uchar col)
	: m_Depth(Depth::GrayScale), m_R(col), m_G(0), m_B(0), m_Value(0.f)
{
}

Pixel::Pixel(uchar r, uchar g, uchar b)
	: m_Depth(Depth::Color), m_R(r), m_G(g), m_B(b), m_Value(0.f)
{
}

Pixel::Pixel(float value)
	: m_Depth(Depth::Floats), m_R(0), m_G(0), m_B(0), m_Value(value)
{

}

uchar Pixel::getColor() const
{
	if (m_Depth != Depth::GrayScale)
		throw "This method can only be used for a gray scale depth.";

	return m_R;
}

float Pixel::getValue() const
{
	if (m_Depth != Depth::Floats)
		throw "This method can only be used for a floats depth.";

	return m_Value;
}

uchar Pixel::getR() const
{
	if (m_Depth != Depth::Color)
		throw "This method can only be used for a color depth.";

	return m_R;
}

uchar Pixel::getG() const
{
	if (m_Depth != Depth::Color)
		throw "This method can only be used for a color depth.";

	return m_G;
}

uchar Pixel::getB() const
{
	if (m_Depth != Depth::Color)
		throw "This method can only be used for a color depth.";

	return m_B;
}


void Pixel::setColor(uchar color)
{
	if (m_Depth != Depth::GrayScale)
		throw "This method can only be used for a gray scale depth.";

	m_R = color;
}

void Pixel::setValue(float value)
{
	if (m_Depth != Depth::Floats)
		throw "This method can only be used for a floats depth.";

	m_Value = value;
}

void Pixel::setR(uchar color)
{
	if (m_Depth != Depth::Color)
		throw "This method can only be used for a color depth.";

	m_R = color;
}

void Pixel::setG(uchar color)
{
	if (m_Depth != Depth::Color)
		throw "This method can only be used for a color depth.";

	m_G = color;
}

void Pixel::setB(uchar color)
{
	if (m_Depth != Depth::Color)
		throw "This method can only be used for a color depth.";

	m_B = color;
}

const Depth& Pixel::getDepth() const
{
	return m_Depth;
}