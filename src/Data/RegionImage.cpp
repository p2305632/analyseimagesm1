#include "Data/RegionImage.h"

#include <stdexcept>

RegionImage::RegionImage(unsigned int width, unsigned int height) noexcept
	: m_Width(width), m_Height(height), m_Pixels(width * height)
{
}

void RegionImage::reset() noexcept
{
	m_Pixels.assign(m_Width * m_Height, std::unordered_set<int>());
	m_Regions.clear();
}

void RegionImage::addRegionToPixel(unsigned int row, unsigned int col, int regionIndex)
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	getPixelRegions(row, col).insert(regionIndex);
	m_Regions[regionIndex].insert(Coordinates(row, col));

}

void RegionImage::setPixelRegion(unsigned int row, unsigned int col, int regionIndex)
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	getPixelRegions(row, col) = { regionIndex };
}

void RegionImage::replaceRegionBy(int fromRegionIndex, int toRegionIndex)
{
	for (auto& regions : m_Pixels)
	{
		regions.erase(fromRegionIndex);
		regions.insert(toRegionIndex);
	}
}

const std::unordered_set<int>& RegionImage::getPixelRegions(unsigned int row, unsigned int col) const
{
	return getPixelRegions(row, col);
}

std::unordered_set<int>& RegionImage::getPixelRegions(unsigned int row, unsigned int col)
{
	if (row >= m_Height || col >= m_Width)
		throw std::out_of_range("Row or col indices are out of range of image bounds.");

	return m_Pixels.at(row * m_Width + col);
}
//const std::unordered_set<Coordinates>& getRegionPixels(int region_Indx) const;

const std::unordered_set<Coordinates>& RegionImage::getRegionPixels(int region_Indx)const
{
	if (m_Regions.contains(region_Indx))
	{
		return m_Regions.at(region_Indx);

	}
	else
	{
		return std::unordered_set<Coordinates>();
	}

}
 

