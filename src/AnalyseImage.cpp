﻿#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <array>

#include "Data/Image.h"
#include "Data/Histogram.h"
#include "Algorithms/ImageProcessor.h"

const int GROWING_NEIGHBOUR_RANGE = 3;

int main(int argc, char** argv)
{
	Image image = { "test_2.jpg", Depth::GrayScale };
	if (image.isEmpty())
	{
		std::cout << "Could not open or find the image" << std::endl;
		return -1;
	}
	image.show("iréférence");

	///* EDGES DETECTION
	Image Fx(3, 3, Depth::Floats);
		std::array<Pixel, 9> dFx = {
			-1.f, 0.f, 1.f,
			-2.f, 0.f, 2.f,
			-1.f, 0.f, 1.f
		};
		Fx.setPixels(dFx);
	Image Fy(3, 3, Depth::Floats);
		std::array<Pixel, 9> dFy = {
			-1.f, -2.f, -1.f,
			 0.f,  0.f,  0.f,
			 1.f,  2.f,  1.f
		};
		Fy.setPixels(dFy);

	Image Gx = ImageProcessor::FilterForValues(image, Fx);
	Image Gy = ImageProcessor::FilterForValues(image, Fy);

	Image edges = ImageProcessor::EuclideanDistance(Gx, Gy);
	edges = ImageProcessor::Threshold(edges, 100, Pixel(uchar(0)), Pixel(uchar(255)));

	/* DILATION
	Image edges_2 = ImageProcessor::Dilatation(edges);
	edges_2.show("Dilatation");
	*/


	///* FILTERING
	Image filter(3, 3, Depth::Floats);
	std::array<Pixel, 9> filterData = {
		1.f / 9.f, 1.f / 9.f, 1.f / 9.f,
		1.f / 9.f, 1.f / 9.f, 1.f / 9.f,
		1.f / 9.f, 1.f / 9.f, 1.f / 9.f
	};
	filter.setPixels(filterData);

	image = ImageProcessor::Equalize(image);
	image = ImageProcessor::Filter(image, filter);
	image = ImageProcessor::Filter(image, filter);
	image = ImageProcessor::Stretch(image, 0, 255 - GROWING_NEIGHBOUR_RANGE - 1);

	///* Paste edges on image

	for (int r = 0; r < edges.getRowsCount(); r++)
	{
		for (int c = 0; c < edges.getColsCount(); c++)
		{
			if (edges.getPixel(r, c).getColor() == 255)
				image.setPixel(r, c, Pixel(uchar(255)));
		}
	}

	image.show("Prepared reference");

	ImageProcessor::RegionGrowing(image, 2000, GROWING_NEIGHBOUR_RANGE).show("Region growing");

	cv::waitKey(0);

	return 0;
}
